def code_morse(text):
    """
    Convert a string into its Morse code equivalent.
    
    :param text: The string to be converted to Morse code.
    :return: The Morse code representation of the string, with Morse code characters
             separated by spaces, or False if input is not a string.
    """
    if not isinstance(text, str):
        return False

   
    morse_dict = {
        'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.',
        'F': '..-.', 'G': '--.', 'H': '....', 'I': '..', 'J': '.---',
        'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.', 'O': '---',
        'P': '.--.', 'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-',
        'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 'Y': '-.--',
        'Z': '--..', '1': '.----', '2': '..---', '3': '...--', '4': '....-',
        '5': '.....', '6': '-....', '7': '--...', '8': '---..', '9': '----.',
        '0': '-----', ', ': '--..--', '.': '.-.-.-', '?': '..--..',
        '/': '-..-.', '-': '-....-', '(': '-.--.', ')': '-.--.-'
    }


    text = text.upper()


    morse_code_output = []

    for char in text:
        if char in morse_dict:
            morse_code_output.append(morse_dict[char])
        elif char == " ":
       
            morse_code_output.append(" ")


    return ' '.join(filter(None, morse_code_output))

 
